/* #region  Smooth scroll to element by Id */
function smoothScroll(id) {
    // console.log(id);
    let target = document.getElementById(id);
    // console.log(target);
    target.scrollIntoView({
      behavior: "smooth",
    });
  }
  /* #endregion */
  